# minipato

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A Sokoban game with level editor.

Developed in a rather short period of time to explain certain concepts.

Tags: game

## Changelog

2016-11-23
- Initial public release


## Download

- [⬇️ minipato.html](dist-archive/minipato.html) *(4.49 KiB)*
